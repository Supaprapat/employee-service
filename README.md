README

Instructions
1. Run with: mvn spring-boot:run
2. Access to Swagger at: http://localhost:8080/swagger-ui/index.html
3. Obtain token from /api/authenticate
4. Available username: user1, user2
5. Authorization header format: Bearer {token retrieved from authentication}
6. Authentication token expiration: 15 minutes

Information:
- Java version: 1.8.0_152
- Database type: H2 in-memory
