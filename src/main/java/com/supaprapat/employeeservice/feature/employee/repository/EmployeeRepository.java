package com.supaprapat.employeeservice.feature.employee.repository;

import com.supaprapat.employeeservice.feature.employee.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

    @Modifying
    @Query("delete from EmployeeEntity where id = ?1")
    void deleteById(Long id);
}
