package com.supaprapat.employeeservice.feature.employee.controller;

import com.supaprapat.employeeservice.feature.authentication.service.AuthenticationService;
import com.supaprapat.employeeservice.feature.common.exception.NotFoundException;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import com.supaprapat.employeeservice.feature.employee.model.Employee;
import com.supaprapat.employeeservice.feature.employee.model.EmployeeRequest;
import com.supaprapat.employeeservice.feature.employee.service.EmployeeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {
    private EmployeeService employeeService;
    private AuthenticationService authenticationService;

    public EmployeeController(EmployeeService employeeService, AuthenticationService authenticationService) {
        this.employeeService = employeeService;
        this.authenticationService = authenticationService;
    }

    @PostMapping("/api/employees")
    public Employee createEmployee(@RequestHeader("Authorization") String authorization,
                                   @RequestBody EmployeeRequest employeeRequest) throws UnauthorizedException {
        verifyAuthorization(authorization);
        return employeeService.createEmployee(employeeRequest);
    }

    @GetMapping("/api/employees")
    public List<Employee> getAllEmployee(@RequestHeader("Authorization") String authorization) throws UnauthorizedException {
        verifyAuthorization(authorization);
        return employeeService.getAllEmployee();
    }

    @GetMapping("/api/employees/{id}")
    public Employee getEmployee(@RequestHeader("Authorization") String authorization,
                                @PathVariable Long id) throws NotFoundException, UnauthorizedException {
        verifyAuthorization(authorization);
        return employeeService.getEmployee(id);
    }

    @PutMapping("/api/employees/{id}")
    public Employee updateEmployee(@RequestHeader("Authorization") String authorization,
                                   @PathVariable Long id,
                                   @RequestBody EmployeeRequest employeeRequest) throws NotFoundException {
        return employeeService.updateEmployee(id, employeeRequest);
    }

    @DeleteMapping("/api/employees/{id}")
    public void deleteEmployee(@RequestHeader("Authorization") String authorization,
                               @PathVariable Long id) throws NotFoundException {
        employeeService.deleteEmployee(id);
    }

    private void verifyAuthorization(String authorization) throws UnauthorizedException {
        String token = authorization.replace("Bearer ", "");
        authenticationService.verifyAuthentication(token);
    }
}
