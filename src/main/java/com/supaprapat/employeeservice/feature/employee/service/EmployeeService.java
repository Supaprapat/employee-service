package com.supaprapat.employeeservice.feature.employee.service;

import com.supaprapat.employeeservice.feature.authentication.service.AuthenticationService;
import com.supaprapat.employeeservice.feature.common.exception.NotFoundException;
import com.supaprapat.employeeservice.feature.employee.entity.EmployeeEntity;
import com.supaprapat.employeeservice.feature.employee.model.Employee;
import com.supaprapat.employeeservice.feature.employee.model.EmployeeRequest;
import com.supaprapat.employeeservice.feature.employee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee createEmployee(EmployeeRequest employeeRequest) {
        // Create the new employee entity and save
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setName(employeeRequest.getName());
        employeeEntity.setPosition(employeeRequest.getPosition());
        employeeRepository.save(employeeEntity);
        return new Employee(employeeEntity.getId(), employeeEntity.getName(), employeeEntity.getPosition());
    }

    public Employee getEmployee(long id) throws NotFoundException {
        // Retrieve the employee
        EmployeeEntity employeeEntity = findEmployeeEntityById(id);

        return new Employee(employeeEntity.getId(), employeeEntity.getName(), employeeEntity.getPosition());
    }

    public List<Employee> getAllEmployee() {
        // Retrieve all employee entities, convert to employee and return
        return employeeRepository.findAll()
                .stream()
                .map(employeeEntity -> new Employee(employeeEntity.getId(), employeeEntity.getName(), employeeEntity.getPosition()))
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id, EmployeeRequest employeeRequest) throws NotFoundException {
        // Retrieve the employee
        EmployeeEntity employeeEntity = findEmployeeEntityById(id);

        // Do the update and save
        employeeEntity.setName(employeeRequest.getName());
        employeeEntity.setPosition(employeeRequest.getPosition());
        employeeRepository.save(employeeEntity);

        return new Employee(employeeEntity.getId(), employeeEntity.getName(), employeeEntity.getPosition());
    }

    public void deleteEmployee(Long id) throws NotFoundException {
        // Check if employee exists
        findEmployeeEntityById(id);

        // Delete the employee
        employeeRepository.deleteById(id);
    }

    private EmployeeEntity findEmployeeEntityById(long id) throws NotFoundException {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("employee id: %s is not found", id)));
    }

}
