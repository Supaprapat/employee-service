package com.supaprapat.employeeservice.feature.employee.model;

import lombok.Data;

@Data
public class EmployeeRequest {
    private String name;
    private String position;
}
