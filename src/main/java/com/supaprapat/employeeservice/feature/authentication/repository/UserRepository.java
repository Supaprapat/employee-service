package com.supaprapat.employeeservice.feature.authentication.repository;


import com.supaprapat.employeeservice.feature.authentication.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByUsername(String username);
}
