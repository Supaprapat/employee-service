package com.supaprapat.employeeservice.feature.authentication.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;

@Service
public class JwtService {

    private static final long TOKEN_TTL = 900000; // token TTL in milliseconds

    private Key key;
    private JwtParser jwtParser;

    public JwtService() {
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        this.jwtParser = Jwts.parserBuilder().setSigningKey(key).build();
    }

    public String generateJwt(String username) {
        long expiration = System.currentTimeMillis() + TOKEN_TTL;

        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(expiration))
                .signWith(key)
                .compact();
    }

    public Claims readJwt(String jws) {
        return jwtParser.parseClaimsJws(jws).getBody();
    }
}
