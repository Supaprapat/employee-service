package com.supaprapat.employeeservice.feature.authentication.service;

import com.supaprapat.employeeservice.feature.authentication.entity.UserEntity;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationRequest;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationResponse;
import com.supaprapat.employeeservice.feature.authentication.repository.UserRepository;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {
    private UserRepository userRepository;
    private JwtService jwtService;

    public AuthenticationService(UserRepository userRepository, JwtService jwtService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
    }

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) throws UnauthorizedException {
        // Check user existence
        Optional<UserEntity> optionalUser = userRepository.findByUsername(authenticationRequest.getUsername());
        if (optionalUser.isPresent()) {
            // Generate token
            return new AuthenticationResponse(jwtService.generateJwt(authenticationRequest.getUsername()));
        } else {
            // UserEntity not exists
            throw new UnauthorizedException("user is unauthorized");
        }
    }

    public void verifyAuthentication(String jwt) throws UnauthorizedException {
        Claims claims = jwtService.readJwt(jwt);
        // Check if user exists
        if (isUserNotExists(claims.getSubject())) {
            throw new UnauthorizedException("user is unauthorized");
        }
    }

    private boolean isUserNotExists(String username) {
        return !userRepository.findByUsername(username).isPresent();
    }
}
