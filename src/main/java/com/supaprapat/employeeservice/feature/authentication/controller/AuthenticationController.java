package com.supaprapat.employeeservice.feature.authentication.controller;

import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationRequest;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationResponse;
import com.supaprapat.employeeservice.feature.authentication.service.AuthenticationService;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

    private AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/api/authenticate")
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest authenticationRequest) throws UnauthorizedException {
        return authenticationService.authenticate(authenticationRequest);
    }
}
