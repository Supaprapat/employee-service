package com.supaprapat.employeeservice.feature.common.exception;

import lombok.Getter;

@Getter
public class UnauthorizedException extends Exception {
    private String message;

    public UnauthorizedException(String message) {
        super(message);
        this.message = message;
    }
}
