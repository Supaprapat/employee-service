package com.supaprapat.employeeservice.feature.common.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends Exception {
    private String message;

    public NotFoundException(String message) {
        super(message);
        this.message = message;
    }
}
