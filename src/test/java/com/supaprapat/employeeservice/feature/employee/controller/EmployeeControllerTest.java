package com.supaprapat.employeeservice.feature.employee.controller;

import com.supaprapat.employeeservice.feature.authentication.service.AuthenticationService;
import com.supaprapat.employeeservice.feature.common.exception.NotFoundException;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import com.supaprapat.employeeservice.feature.employee.model.Employee;
import com.supaprapat.employeeservice.feature.employee.model.EmployeeRequest;
import com.supaprapat.employeeservice.feature.employee.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {
    private EmployeeController employeeController;

    @Mock
    private EmployeeService employeeService;

    @Mock
    private AuthenticationService authenticationService;

    private String token = "Bearer sample_token";
    private String employeeName = "test_employee";
    private String employeePosition = "Developer";
    private long employeeId = 0L;
    private EmployeeRequest employeeRequest;
    private Employee employee;

    @Before
    public void setup() {
        employeeController = new EmployeeController(employeeService, authenticationService);

        employeeRequest = new EmployeeRequest();
        employeeRequest.setName(employeeName);
        employeeRequest.setPosition(employeePosition);

        employee = new Employee(0L, employeeName, employeePosition);
    }

    @Test
    public void createEmployee_success() throws UnauthorizedException {
        when(employeeService.createEmployee(employeeRequest)).thenReturn(employee);

        Employee actual = employeeController.createEmployee(token, employeeRequest);

        assertEquals(employeeName, actual.getName());
        assertEquals(employeePosition, actual.getPosition());
    }

    @Test
    public void getAllEmployee_success() throws UnauthorizedException {
        when(employeeService.getAllEmployee()).thenReturn(Collections.singletonList(employee));

        List<Employee> actual = employeeController.getAllEmployee(token);

        assertEquals(1, actual.size());
        assertEquals(employeeName, actual.get(0).getName());
        assertEquals(employeePosition, actual.get(0).getPosition());
    }

    @Test
    public void getEmployee_success() throws NotFoundException, UnauthorizedException {
        when(employeeService.getEmployee(employeeId)).thenReturn(employee);

        Employee actual = employeeController.getEmployee(token, employeeId);

        assertEquals(employeeId, actual.getId());
        assertEquals(employeeName, actual.getName());
        assertEquals(employeePosition, actual.getPosition());
    }

    @Test
    public void updateEmployee_success() throws NotFoundException {
        String name = "new_employee_name";
        String position = "Senior Developer";

        employeeRequest.setName(name);
        employeeRequest.setPosition(position);

        employee.setName(name);
        employee.setPosition(position);

        when(employeeService.updateEmployee(employeeId, employeeRequest)).thenReturn(employee);

        Employee actual = employeeController.updateEmployee(token, employeeId, employeeRequest);

        assertEquals(employeeId, actual.getId());
        assertEquals(name, actual.getName());
        assertEquals(position, actual.getPosition());
    }

    @Test
    public void deleteEmployee_success() throws NotFoundException {
        doNothing().when(employeeService).deleteEmployee(employeeId);

        employeeController.deleteEmployee(token, employeeId);
        verify(employeeService, times(1)).deleteEmployee(employeeId);
    }
}
