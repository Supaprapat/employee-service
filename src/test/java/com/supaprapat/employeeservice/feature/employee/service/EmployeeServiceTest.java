package com.supaprapat.employeeservice.feature.employee.service;

import com.supaprapat.employeeservice.feature.common.exception.NotFoundException;
import com.supaprapat.employeeservice.feature.employee.entity.EmployeeEntity;
import com.supaprapat.employeeservice.feature.employee.model.Employee;
import com.supaprapat.employeeservice.feature.employee.model.EmployeeRequest;
import com.supaprapat.employeeservice.feature.employee.repository.EmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
    private EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    private String token = "Bearer sample_token";
    private String employeeName = "test_employee";
    private String employeePosition = "Developer";
    private long employeeId = 0L;
    private EmployeeRequest employeeRequest;
    private Employee employee;
    private EmployeeEntity employeeEntity;

    @Before
    public void setup() {
        employeeService = new EmployeeService(employeeRepository);

        employeeRequest = new EmployeeRequest();
        employeeRequest.setName(employeeName);
        employeeRequest.setPosition(employeePosition);

        employee = new Employee(employeeId, employeeName, employeePosition);

        employeeEntity = new EmployeeEntity();
        employeeEntity.setId(employeeId);
        employeeEntity.setName(employeeName);
        employeeEntity.setPosition(employeePosition);

        when(employeeRepository.findById(employeeId)).thenReturn(Optional.of(employeeEntity));
    }

    @Test
    public void createEmployee_success() {
        when(employeeRepository.save(employeeEntity)).thenReturn(employeeEntity);

        Employee actual = employeeService.createEmployee(employeeRequest);

        assertEquals(employeeId, actual.getId());
        assertEquals(employeeName, actual.getName());
        assertEquals(employeePosition, actual.getPosition());
    }

    @Test
    public void getEmployee_success() throws NotFoundException {
        Employee actual = employeeService.getEmployee(employeeId);

        assertEquals(employeeId, actual.getId());
        assertEquals(employeeName, actual.getName());
        assertEquals(employeePosition, actual.getPosition());
    }

    @Test
    public void getAllEmployee_success() {
        when(employeeRepository.findAll()).thenReturn(Collections.singletonList(employeeEntity));

        List<Employee> actual = employeeService.getAllEmployee();

        assertEquals(1, actual.size());
        assertEquals(employeeId, actual.get(0).getId());
        assertEquals(employeeName, actual.get(0).getName());
        assertEquals(employeePosition, actual.get(0).getPosition());
    }

    @Test
    public void updateEmployee_success() throws NotFoundException {
        String name = "new_name";
        String position = "Senior Developer";

        employeeEntity.setName(name);
        employeeEntity.setPosition(position);

        employeeRequest.setName(name);
        employeeRequest.setPosition(position);

        when(employeeRepository.save(employeeEntity)).thenReturn(employeeEntity);

        Employee actual = employeeService.updateEmployee(employeeId, employeeRequest);

        assertEquals(employeeId, actual.getId());
        assertEquals(name, actual.getName());
        assertEquals(position, actual.getPosition());
    }

    @Test
    public void deleteEmployee_success() throws NotFoundException {
        doNothing().when(employeeRepository).deleteById(employeeId);

        employeeService.deleteEmployee(employeeId);
        verify(employeeRepository, times(1)).deleteById(employeeId);
    }

}
