package com.supaprapat.employeeservice.feature.authentication.controller;

import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationRequest;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationResponse;
import com.supaprapat.employeeservice.feature.authentication.service.AuthenticationService;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationControllerTest {

    private AuthenticationController authenticationController;

    @Mock
    private AuthenticationService authenticationService;

    @Test
    public void authenticate_success() throws UnauthorizedException {
        String token = "sample_token";

        authenticationController = new AuthenticationController(authenticationService);
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("test_user");
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(token);

        when(authenticationService.authenticate(any(AuthenticationRequest.class))).thenReturn(authenticationResponse);

        AuthenticationResponse actual = authenticationController.authenticate(authenticationRequest);
        assertEquals(token, actual.getToken());
    }
}
