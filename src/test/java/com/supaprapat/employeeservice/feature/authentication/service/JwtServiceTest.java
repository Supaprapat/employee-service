package com.supaprapat.employeeservice.feature.authentication.service;

import io.jsonwebtoken.Claims;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class JwtServiceTest {

    private JwtService jwtService;

    private String username = "test_user";

    @Before
    public void setup() {
        jwtService = new JwtService();
    }

    @Test
    public void generateJwt_success() {
        String actual = jwtService.generateJwt(username);

        assertNotNull(actual);
        assertEquals(username, jwtService.readJwt(actual).getSubject());
    }

    @Test
    public void readJwt_success() {
        String token = jwtService.generateJwt(username);

        Claims actual = jwtService.readJwt(token);

        assertEquals(username, actual.getSubject());
    }
}
