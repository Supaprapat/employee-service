package com.supaprapat.employeeservice.feature.authentication.service;

import com.supaprapat.employeeservice.feature.authentication.entity.UserEntity;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationRequest;
import com.supaprapat.employeeservice.feature.authentication.model.AuthenticationResponse;
import com.supaprapat.employeeservice.feature.authentication.repository.UserRepository;
import com.supaprapat.employeeservice.feature.common.exception.UnauthorizedException;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceTest {

    private AuthenticationService authenticationService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtService jwtService;

    private String username = "Erik";
    private String token = "sample_token";
    private AuthenticationRequest authenticationRequest;
    private DefaultClaims claims;

    @Before
    public void setup() {
        authenticationService = new AuthenticationService(userRepository, jwtService);

        UserEntity userEntity = new UserEntity();
        userEntity.setId(99L);
        userEntity.setUsername(username);
        Optional<UserEntity> optionalUserEntity = Optional.of(userEntity);

        authenticationRequest = new AuthenticationRequest(username);

        claims = new DefaultClaims();
        claims.setSubject(username);

        when(userRepository.findByUsername(eq(username))).thenReturn(optionalUserEntity);
        when(jwtService.generateJwt(eq(username))).thenReturn(token);

        when(jwtService.readJwt(eq(token))).thenReturn(claims);
    }

    @Test
    public void authenticate_success() throws UnauthorizedException {
        AuthenticationResponse actual = authenticationService.authenticate(authenticationRequest);

        assertEquals(token, actual.getToken());
    }

    @Test
    public void authenticate_failed_unauthorized() {
        when(userRepository.findByUsername(eq(username))).thenReturn(Optional.empty());

        try {
            authenticationService.authenticate(authenticationRequest);
            fail("It should be an UnauthorizedException thrown.");
        } catch (UnauthorizedException ex) {
            assertEquals("user is unauthorized", ex.getMessage());
        } catch (Exception ex) {
            fail("It should be an UnauthorizedException thrown.");
        }
    }

    @Test
    public void verifyAuthentication_success() {
        try {
            authenticationService.verifyAuthentication(token);
            verify(jwtService, times(1)).readJwt(eq(token));
            verify(userRepository, times(1)).findByUsername(eq(username));
        } catch (Exception e) {
            fail("It should be no exception thrown.");
        }
    }

    @Test
    public void verifyAuthentication_failed_userNotExists() {
        when(userRepository.findByUsername(eq(username))).thenReturn(Optional.empty());

        try {
            authenticationService.verifyAuthentication(token);
            verify(jwtService, times(1)).readJwt(eq(token));
            verify(userRepository, times(1)).findByUsername(eq(username));
            fail("It should be an UnauthorizedException thrown.");
        } catch (UnauthorizedException ex) {
            assertEquals("user is unauthorized", ex.getMessage());
        } catch (Exception ex) {
            fail("It should be an UnauthorizedException thrown.");
        }
    }
}
